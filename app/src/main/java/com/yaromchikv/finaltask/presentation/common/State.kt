package com.yaromchikv.finaltask.presentation.common

sealed class State<out T> {
    class Ready<T>(val data: T?) : State<T>()
    class Error(val error: Throwable?) : State<Nothing>()
    object Loading : State<Nothing>()
    object Idle : State<Nothing>()
}