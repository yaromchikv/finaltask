package com.yaromchikv.finaltask.presentation.feature.form

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yaromchikv.finaltask.domain.model.ResponseModel
import com.yaromchikv.finaltask.domain.model.metadata.FieldModel
import com.yaromchikv.finaltask.domain.model.metadata.MetadataModel
import com.yaromchikv.finaltask.domain.usecase.GenerateFormUseCase
import com.yaromchikv.finaltask.domain.usecase.GetMetadataUseCase
import com.yaromchikv.finaltask.domain.usecase.PostFormUseCase
import com.yaromchikv.finaltask.presentation.common.State
import com.yaromchikv.finaltask.utils.Result
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class FormViewModel @Inject constructor(
    private val getMetadataUseCase: GetMetadataUseCase,
    private val postFormUseCase: PostFormUseCase,
    private val generateFormUseCase: GenerateFormUseCase
) : ViewModel() {

    private val _metadata = MutableStateFlow<State<MetadataModel>>(State.Idle)
    val metadata = _metadata.asStateFlow()

    private val _response = MutableStateFlow<State<ResponseModel>>(State.Idle)
    val response = _response.asStateFlow()

    init {
        fetchMetadata()
    }

    private fun fetchMetadata() {
        viewModelScope.launch {
            _metadata.value = State.Loading

            val metadata = getMetadataUseCase()
            _metadata.value = when (metadata) {
                is Result.Success -> State.Ready(metadata.data)
                is Result.Error -> State.Error(metadata.error)
            }
        }
    }

    fun postForm(fieldsList: List<FieldModel>) {
        viewModelScope.launch {
            _response.value = State.Loading

            val form = generateFormUseCase(fieldsList)
            val response = postFormUseCase(form)
            _response.value = when (response) {
                is Result.Success -> State.Ready(response.data)
                is Result.Error -> State.Error(response.error)
            }
        }
    }

    fun resetTheResponse() {
        _response.value = State.Idle
    }
}