package com.yaromchikv.finaltask.presentation.feature.form

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.bumptech.glide.Glide
import com.yaromchikv.finaltask.R
import com.yaromchikv.finaltask.databinding.FragmentFormBinding
import com.yaromchikv.finaltask.domain.model.metadata.MetadataModel
import com.yaromchikv.finaltask.presentation.common.State
import dagger.android.support.DaggerFragment
import java.net.UnknownHostException
import javax.inject.Inject
import kotlinx.coroutines.flow.collectLatest

class FormFragment : DaggerFragment(R.layout.fragment_form) {

    private val binding by viewBinding(FragmentFormBinding::bind)

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by viewModels<FormViewModel> { viewModelFactory }

    @Inject
    lateinit var formAdapter: FormAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapter()
        setupMetadataCollector()
        setupResponseCollector()

        binding.sendButton.setOnClickListener {
            viewModel.postForm(formAdapter.currentList)
        }
    }

    private fun setupAdapter() {
        binding.recyclerView.apply {
            adapter = formAdapter
            layoutManager = LinearLayoutManager(requireContext())
            isNestedScrollingEnabled = true
        }
    }

    private fun setupMetadataCollector() {
        lifecycleScope.launchWhenStarted {
            viewModel.metadata.collectLatest {
                when (it) {
                    is State.Ready -> {
                        changeProgressBarState(false)
                        if (it.data != null) {
                            setMetadata(it.data)
                        } else {
                            showError(null)
                        }
                    }
                    is State.Loading -> {
                        changeProgressBarState(true)
                    }
                    is State.Error -> {
                        changeProgressBarState(false)
                        showError(it.error)
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun setupResponseCollector() {
        lifecycleScope.launchWhenStarted {
            viewModel.response.collectLatest {
                when (it) {
                    is State.Ready -> {
                        changeProgressBarState(false)
                        showDialog(
                            title = getString(R.string.result),
                            message = it.data?.result ?: getString(R.string.no_data_available)
                        )
                    }
                    is State.Loading -> {
                        changeProgressBarState(true)
                    }
                    is State.Error -> {
                        changeProgressBarState(false)
                        showError(it.error)
                    }
                    else -> Unit
                }
            }
        }
    }

    private fun setMetadata(metadata: MetadataModel) {
        with(binding) {
            title.text = metadata.title
            title.isSelected = true
            sendButton.isVisible = true
        }
        loadImage(metadata.image)
        formAdapter.submitList(metadata.fields)
    }

    private fun changeProgressBarState(isVisible: Boolean) {
        binding.progressBar.isVisible = isVisible
        binding.sendButton.isEnabled = !isVisible
    }

    private fun loadImage(url: String) {
        Glide.with(requireContext())
            .load(url)
            .into(binding.image)
    }

    private fun showDialog(title: String, message: String) =
        AlertDialog.Builder(requireContext())
            .setTitle(title)
            .setMessage(message)
            .setOnDismissListener { viewModel.resetTheResponse() }
            .create()
            .show()

    private fun showError(error: Throwable?) {
        val message = when (error) {
            is UnknownHostException -> getString(R.string.connection_error)
            null -> getString(R.string.unknown_error)
            else -> error.localizedMessage
        }
        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
    }

    companion object {
        fun newInstance() = FormFragment()
    }
}