package com.yaromchikv.finaltask.presentation

import android.os.Bundle
import androidx.fragment.app.commitNow
import com.yaromchikv.finaltask.R
import com.yaromchikv.finaltask.presentation.feature.form.FormFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager.commitNow {
                replace(R.id.container, FormFragment.newInstance())
            }
        }
    }
}