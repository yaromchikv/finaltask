package com.yaromchikv.finaltask.presentation.feature.form

import android.content.Context
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yaromchikv.finaltask.R
import com.yaromchikv.finaltask.databinding.FieldSpinnerBinding
import com.yaromchikv.finaltask.databinding.FieldTextBinding
import com.yaromchikv.finaltask.domain.model.metadata.FieldModel
import com.yaromchikv.finaltask.domain.model.metadata.FieldType
import javax.inject.Inject

class FormAdapter @Inject constructor(
    private val context: Context
) : ListAdapter<FieldModel, FormAdapter.FieldViewHolder>(DiffCallback) {

    abstract class FieldViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        abstract fun bind(field: FieldModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FieldViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            LIST_VIEW_TYPE -> {
                val binding = FieldSpinnerBinding.inflate(layoutInflater, parent, false)
                ListFieldViewHolder(binding)
            }
            else -> {
                val binding = FieldTextBinding.inflate(layoutInflater, parent, false)
                TextFieldViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: FieldViewHolder, position: Int) {
        when (holder.itemViewType) {
            LIST_VIEW_TYPE -> {
                val viewHolder = holder as ListFieldViewHolder
                viewHolder.bind(getItem(position))
            }
            TEXT_VIEW_TYPE -> {
                val viewHolder = holder as TextFieldViewHolder
                viewHolder.bind(getItem(position))
            }
        }
    }

    inner class ListFieldViewHolder(private val binding: FieldSpinnerBinding) :
        FieldViewHolder(binding.root) {

        override fun bind(field: FieldModel) {
            with(binding) {
                fieldName.text = field.title

                val listOfValues = field.values?.values?.toList()
                if (listOfValues != null) {
                    val spinnerAdapter = ArrayAdapter(context, R.layout.spinner_item, listOfValues)
                    val position = spinnerAdapter.getPosition(field.values[field.value])

                    listField.apply {
                        adapter = spinnerAdapter
                        setSelection(position)
                        onItemSelectedListener = onItemSelectedListener(field)
                    }
                }
            }
        }

        private fun onItemSelectedListener(field: FieldModel) =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    val value = parent.getItemAtPosition(position).toString()
                    val key = field.values?.keys?.first { value == field.values[it] }
                    currentList[absoluteAdapterPosition].value = key
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {}
            }
    }

    inner class TextFieldViewHolder(private val binding: FieldTextBinding) :
        FieldViewHolder(binding.root) {

        override fun bind(field: FieldModel) {
            with(binding) {
                fieldName.text = field.title
                textField.inputType = if (field.type == FieldType.NUMERIC) {
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                } else {
                    InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_NORMAL
                }

                textField.apply {
                    setText(field.value)
                    doAfterTextChanged {
                        field.value = it.toString()
                    }
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).type) {
            FieldType.LIST -> LIST_VIEW_TYPE
            FieldType.NUMERIC -> TEXT_VIEW_TYPE
            FieldType.TEXT -> TEXT_VIEW_TYPE
        }
    }

    companion object {
        private const val TEXT_VIEW_TYPE = 0
        private const val LIST_VIEW_TYPE = 1

        object DiffCallback : DiffUtil.ItemCallback<FieldModel>() {
            override fun areItemsTheSame(oldItem: FieldModel, newItem: FieldModel) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: FieldModel, newItem: FieldModel) =
                oldItem == newItem
        }
    }
}