package com.yaromchikv.finaltask.data.repository

import com.yaromchikv.finaltask.data.api.TestApi
import com.yaromchikv.finaltask.data.mapper.DataMapper.toFormDto
import com.yaromchikv.finaltask.data.mapper.DataMapper.toMetadataModel
import com.yaromchikv.finaltask.data.mapper.DataMapper.toResponseModel
import com.yaromchikv.finaltask.domain.model.FormModel
import com.yaromchikv.finaltask.domain.model.ResponseModel
import com.yaromchikv.finaltask.domain.model.metadata.MetadataModel
import com.yaromchikv.finaltask.domain.repository.FormRepository
import com.yaromchikv.finaltask.utils.Result
import javax.inject.Inject

class FormRepositoryImpl @Inject constructor(
    private val apiService: TestApi
) : FormRepository {

    override suspend fun getMetadata(): Result<MetadataModel> {
        return try {
            val metadataDto = apiService.getMetadata().body()
            val metadataModel = metadataDto?.toMetadataModel()
            Result.Success(metadataModel)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun postForm(form: FormModel): Result<ResponseModel> {
        return try {
            val formDto = form.toFormDto()
            val responseDto = apiService.postForm(formDto).body()
            val responseModel = responseDto?.toResponseModel()
            Result.Success(responseModel)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}