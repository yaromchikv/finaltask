package com.yaromchikv.finaltask.data.dto.response.get


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.yaromchikv.finaltask.domain.model.metadata.FieldModel

@JsonClass(generateAdapter = true)
data class FieldDto(
    @Json(name = "title") val title: String,
    @Json(name = "name") val name: String,
    @Json(name = "type") val type: String,
    @Json(name = "values") val values: Map<String, String>?
)