package com.yaromchikv.finaltask.data.mapper

import com.yaromchikv.finaltask.data.dto.request.FormDto
import com.yaromchikv.finaltask.data.dto.response.get.MetadataDto
import com.yaromchikv.finaltask.data.dto.response.post.ResponseDto
import com.yaromchikv.finaltask.domain.model.FormModel
import com.yaromchikv.finaltask.domain.model.ResponseModel
import com.yaromchikv.finaltask.domain.model.metadata.FieldModel
import com.yaromchikv.finaltask.domain.model.metadata.FieldType
import com.yaromchikv.finaltask.domain.model.metadata.MetadataModel

object DataMapper {

    fun MetadataDto.toMetadataModel(): MetadataModel {
        val fields = mutableListOf<FieldModel>()
        for (i in 0 until this.fields.size) {
            val fieldDto = this.fields[i]
            fields.add(
                FieldModel(
                    id = i,
                    title = fieldDto.title,
                    name = fieldDto.name,
                    type = FieldType.valueOf(fieldDto.type),
                    values = fieldDto.values,
                    value = fieldDto.values?.keys?.first() ?: ""
                )
            )
        }
        return MetadataModel(
            title = this.title,
            image = this.image,
            fields = fields
        )
    }

    fun FormModel.toFormDto() = FormDto(form = this.form)

    fun ResponseDto.toResponseModel() = ResponseModel(result = this.result)
}