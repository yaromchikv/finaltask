package com.yaromchikv.finaltask.data.dto.response.get

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.yaromchikv.finaltask.domain.model.metadata.MetadataModel

@JsonClass(generateAdapter = true)
data class MetadataDto(
    @Json(name = "title") val title: String,
    @Json(name = "image") val image: String,
    @Json(name = "fields") val fields: List<FieldDto>
)