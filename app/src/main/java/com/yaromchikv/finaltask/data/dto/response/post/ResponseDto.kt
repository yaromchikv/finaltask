package com.yaromchikv.finaltask.data.dto.response.post


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ResponseDto(
    @Json(name = "result") val result: String
)