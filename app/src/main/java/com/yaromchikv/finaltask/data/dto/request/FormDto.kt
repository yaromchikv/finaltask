package com.yaromchikv.finaltask.data.dto.request

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import com.yaromchikv.finaltask.domain.model.FormModel

@JsonClass(generateAdapter = true)
data class FormDto(
    @Json(name = "form") val form: Map<String, String>
)