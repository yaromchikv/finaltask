package com.yaromchikv.finaltask.data.api

import com.yaromchikv.finaltask.data.dto.request.FormDto
import com.yaromchikv.finaltask.data.dto.response.get.MetadataDto
import com.yaromchikv.finaltask.data.dto.response.post.ResponseDto
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface TestApi {

    @GET("tt/meta/")
    suspend fun getMetadata(): Response<MetadataDto>

    @POST("tt/data/")
    suspend fun postForm(
        @Body form: FormDto
    ): Response<ResponseDto>
}