package com.yaromchikv.finaltask.di.modules.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yaromchikv.finaltask.presentation.common.ViewModelFactory
import com.yaromchikv.finaltask.presentation.feature.form.FormViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(FormViewModel::class)
    abstract fun bindFormViewModel(viewModel: FormViewModel): ViewModel
}