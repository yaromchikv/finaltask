package com.yaromchikv.finaltask.di.modules

import android.app.Application
import android.content.Context
import com.yaromchikv.finaltask.di.modules.data.NetworkModule
import com.yaromchikv.finaltask.di.modules.data.RepositoryModule
import com.yaromchikv.finaltask.di.modules.domain.UseCaseModule
import com.yaromchikv.finaltask.di.modules.presentation.ActivityBuildersModule
import com.yaromchikv.finaltask.di.modules.presentation.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [
        ActivityBuildersModule::class,
        ViewModelModule::class,
        NetworkModule::class,
        RepositoryModule::class,
        UseCaseModule::class
    ]
)
object AppModule {

    @Singleton
    @Provides
    fun provideContext(app: Application): Context = app.applicationContext
}