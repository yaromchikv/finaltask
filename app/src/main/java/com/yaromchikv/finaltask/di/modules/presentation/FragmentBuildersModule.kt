package com.yaromchikv.finaltask.di.modules.presentation

import com.yaromchikv.finaltask.presentation.feature.form.FormFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeFormFragment(): FormFragment
}