package com.yaromchikv.finaltask.di.modules.domain

import com.yaromchikv.finaltask.domain.repository.FormRepository
import com.yaromchikv.finaltask.domain.usecase.GenerateFormUseCase
import com.yaromchikv.finaltask.domain.usecase.GetMetadataUseCase
import com.yaromchikv.finaltask.domain.usecase.PostFormUseCase
import dagger.Module
import dagger.Provides

@Module
object UseCaseModule {

    @Provides
    fun provideGetMetadataUseCase(repository: FormRepository) = GetMetadataUseCase(repository)

    @Provides
    fun providePostFormUseCase(repository: FormRepository) = PostFormUseCase(repository)

    @Provides
    fun provideGenerateFormUseCase() = GenerateFormUseCase()
}