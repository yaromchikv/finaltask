package com.yaromchikv.finaltask.di.modules.data

import com.yaromchikv.finaltask.data.api.TestApi
import com.yaromchikv.finaltask.data.repository.FormRepositoryImpl
import com.yaromchikv.finaltask.domain.repository.FormRepository
import dagger.Module
import dagger.Provides

@Module
object RepositoryModule {

    @Provides
    fun provideRepository(
        apiService: TestApi
    ): FormRepository = FormRepositoryImpl(apiService)
}