package com.yaromchikv.finaltask.domain.model.metadata

data class FieldModel(
    val id: Int,
    val title: String,
    val name: String,
    val type: FieldType,
    var value: String?,
    val values: Map<String, String>?
)