package com.yaromchikv.finaltask.domain.usecase

import com.yaromchikv.finaltask.domain.model.metadata.MetadataModel
import com.yaromchikv.finaltask.domain.repository.FormRepository
import com.yaromchikv.finaltask.utils.Result

class GetMetadataUseCase(private val repository: FormRepository) {
    suspend operator fun invoke(): Result<MetadataModel> {
        return repository.getMetadata()
    }
}
