package com.yaromchikv.finaltask.domain.model

data class FormModel(
    val form: Map<String, String>
)