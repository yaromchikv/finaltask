package com.yaromchikv.finaltask.domain.model

data class ResponseModel(
    val result: String
)