package com.yaromchikv.finaltask.domain.model.metadata

enum class FieldType {
    TEXT, NUMERIC, LIST
}