package com.yaromchikv.finaltask.domain.model.metadata

data class MetadataModel(
    val title: String,
    val image: String,
    val fields: List<FieldModel>
)