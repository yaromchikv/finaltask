package com.yaromchikv.finaltask.domain.usecase

import com.yaromchikv.finaltask.domain.model.FormModel
import com.yaromchikv.finaltask.domain.model.metadata.FieldModel
import com.yaromchikv.finaltask.domain.model.metadata.FieldType

class GenerateFormUseCase {
    operator fun invoke(fieldsList: List<FieldModel>): FormModel {
        val map = mutableMapOf<String, String>()
        fieldsList.forEach { field ->
            map[field.name] = when (field.type) {
                FieldType.TEXT -> field.value ?: ""
                FieldType.NUMERIC -> (field.value?.toDoubleOrNull() ?: 0.0).toString()
                FieldType.LIST -> field.value ?: "none"
            }
        }
        return FormModel(form = map)
    }
}
