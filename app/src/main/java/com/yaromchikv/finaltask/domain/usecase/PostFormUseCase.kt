package com.yaromchikv.finaltask.domain.usecase

import com.yaromchikv.finaltask.domain.model.FormModel
import com.yaromchikv.finaltask.domain.model.ResponseModel
import com.yaromchikv.finaltask.domain.repository.FormRepository
import com.yaromchikv.finaltask.utils.Result

class PostFormUseCase(private val repository: FormRepository) {
    suspend operator fun invoke(form: FormModel): Result<ResponseModel> {
        return repository.postForm(form)
    }
}
