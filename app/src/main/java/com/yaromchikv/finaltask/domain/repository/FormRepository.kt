package com.yaromchikv.finaltask.domain.repository

import com.yaromchikv.finaltask.domain.model.FormModel
import com.yaromchikv.finaltask.domain.model.ResponseModel
import com.yaromchikv.finaltask.domain.model.metadata.MetadataModel
import com.yaromchikv.finaltask.utils.Result

interface FormRepository {
    suspend fun getMetadata(): Result<MetadataModel>
    suspend fun postForm(form: FormModel): Result<ResponseModel>
}